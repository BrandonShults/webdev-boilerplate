import '../../sass/main.scss';
import ReactDom from 'react-dom';
import React from 'react';
import RootComponent from '../components/root-component.jsx';

ReactDom.render(<RootComponent />, document.getElementById('content'));

