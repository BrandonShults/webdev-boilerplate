import React from 'react';

export default class RootComponent extends React.Component {
  render() {
    return (
      <div className="react-root">
        <h1>Some Boilerplate</h1>
        <ul>
          <li>webpack</li>
          <li>babel</li>
          <li>react</li>
          <li>sass</li>
        </ul>
      </div>
    )
  }
}